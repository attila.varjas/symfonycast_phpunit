<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\EnclosureBuilderService;
use App\Entity\Dinosaur;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class EnclosureBuilderServiceIntegrationTest extends KernelTestCase
{
    public function setUp()
    {
        self::bootKernel();
        $this->truncateEntities();
    }

    public function testItBuildsEnclosureWithDefaultSpecifications()
    {
        $enclosureBuilderService = self::$container->get('test.' . EnclosureBuilderService::class);
        $em = $this->getEntityManager();

        $enclosureBuilderService->buildEnclosure();

        $count = (int) $em->getRepository(Dinosaur::class)
        ->createQueryBuilder('d')
        ->select('COUNT(d.id)')
        ->getQuery()
        ->getSingleScalarResult();
        
        $this->assertSame(3, $count, 'Amount of dinosaurs is not the same');
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return self::$container->get('doctrine')->getManager();
    }

    private function truncateEntities()
    {
        $purger = new ORMPurger($this->getEntityManager());
        $purger->purge();
    }
}