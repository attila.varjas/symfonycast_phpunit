<?php

namespace App\Tests\Factory;

use PHPUnit\Framework\TestCase;
use App\Entity\Dinosaur;
use App\Factory\DinosaurFactory;
use App\Service\DinosaurLengthHelper;

class dinosaurFactoryTest extends TestCase
{
    /**
     * @var DinosaurFactory;
     */
    private $factory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $mockLengthHelper;

    public function setUp()
    {
        $this->mockLengthHelper = $this->createMock(DinosaurLengthHelper::class);
        $this->factory = new DinosaurFactory($this->mockLengthHelper);
    }

    public function testItGrowsAVelociraptor()
    {
        $dinosaur = $this->factory->growVelociraptor(5);

        $this->assertInstanceOf(Dinosaur::class, $dinosaur);
        $this->assertInternalType('string', $dinosaur->getGenus());
        $this->assertSame('Velociraptor', $dinosaur->getGenus());
        $this->assertSame(5, $dinosaur->getLength());
    }

    public function testItGrowsATricerotreps()
    {
        $this->markTestIncomplete();
    }

    /**
     * @dataProvider getSpecificationTests
     */
    public function testItGrowsFromSpecification(string $spec, bool $isCarnivorous)
    {
        $this->mockLengthHelper->expects($this->once())
            ->method('getLengthFromSpecification')
            ->with($spec)
            ->willReturn(20);

        $dinosaur = $this->factory->growFromSpecification($spec);
    
        $this->assertSame($isCarnivorous, $dinosaur->getIsCarnivorous());
        $this->assertSame(20, $dinosaur->getLength());
    }

    public function getSpecificationTests()
    {
        return [
            // specification, is carnivorous
            ['large carnivorous dinosaur', true],
            ['give me all the coookies', false],
            ['large herbivhore dinosaur', false],
        ];
    }
}