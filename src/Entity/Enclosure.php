<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Exception\NotABuffetException;
use App\Exception\DinosaursAreRunningRampantException;
use App\Entity\Security;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnclosureRepository")
 */
class Enclosure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dinosaur", mappedBy="enclosure", cascade={"persist"})
     */
    private $dinosaurs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Security", mappedBy="enclosure", cascade={"persist"})
     */
    private $securities;

    public function __construct(bool $withBasicSecurity = false)
    {
        $this->dinosaurs = new ArrayCollection();
        $this->securities = new ArrayCollection();

        if ($withBasicSecurity === true) {
            $security = new Security();
            $security->setName('Basic')->setIsActive(true);
            $this->addSecurity($security);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Dinosaur[]
     */
    public function getDinosaurs(): Collection
    {
        return $this->dinosaurs;
    }

    public function addDinosaur(Dinosaur $dinosaur): self
    {
        if (!$this->canAddDinosaur($dinosaur)) {
            throw new NotABuffetException();
        }

        if (!$this->isSecurityActive()) {
            throw new DinosaursAreRunningRampantException();
        }

        //if (!$this->dinosaurs->contains($dinosaur)) {
            $this->dinosaurs[] = $dinosaur;
            $dinosaur->setEnclosure($this);
        //}

        return $this;
    }

    public function removeDinosaur(Dinosaur $dinosaur): self
    {
        if ($this->dinosaurs->contains($dinosaur)) {
            $this->dinosaurs->removeElement($dinosaur);
            // set the owning side to null (unless already changed)
            if ($dinosaur->getEnclosure() === $this) {
                $dinosaur->setEnclosure(null);
            }
        }

        return $this;
    }

    private function canAddDinosaur(Dinosaur $dinosaur): bool
    {
        return count($this->dinosaurs) === 0 || $this->getDinosaurs()->first()->getIsCarnivorous() === $dinosaur->getIsCarnivorous();
    }

    /**
     * @return Collection|Security[]
     */
    public function getSecurities(): Collection
    {
        return $this->securities;
    }

    public function addSecurity(Security $security): self
    {
        if (!$this->securities->contains($security)) {
            $this->securities[] = $security;
            $security->setEnclosure($this);
        }

        return $this;
    }

    public function removeSecurity(Security $security): self
    {
        if ($this->securities->contains($security)) {
            $this->securities->removeElement($security);
            // set the owning side to null (unless already changed)
            if ($security->getEnclosure() === $this) {
                $security->setEnclosure(null);
            }
        }

        return $this;
    }

    public function isSecurityActive()
    {
        foreach ($this->securities as $security) {
            if ($security->getIsActive() === true) {
                return true;
            }
        }

        return false;
    }
}
