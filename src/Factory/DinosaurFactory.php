<?php

namespace App\Factory;

use App\Entity\Dinosaur;
use App\Service\DinosaurLengthHelper;

class DinosaurFactory
{
    private $dinosaurLengthHelper;

    public function __construct(DinosaurLengthHelper $dinosaurLengthHelper) {
        $this->dinosaurLengthHelper = $dinosaurLengthHelper;
    }

    public function growVelociraptor(int $length): Dinosaur
    {
        return $this->createDinosaur('Velociraptor', true, 5);
    }

    private function createDinosaur(string $genus, bool $isCarnivorous, int $length): Dinosaur
    {
        $dinosaur = new Dinosaur($genus, $isCarnivorous);
        $dinosaur->setLength($length);

        return $dinosaur;
    }

    public function growFromSpecification(string $specification): Dinosaur
    {
        $codeName = 'InG-' . random_int(1, 99999);
        $length = $this->dinosaurLengthHelper->getLengthFromSpecification($specification);
    
        $isCarnivorous = false;

        if(strpos($specification, 'carnivorous') !== false) {
            $isCarnivorous = true;
        }

        return $this->createDinosaur($codeName, $isCarnivorous, $length);
    }
}